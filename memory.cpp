#include <Windows.h>
#include <iostream>
#include <TlHelp32.h>
#include <vector>
#include "memory.h"


bool CMemoryManager::Attach(const std::string& strProcessName)
{
	// create Snapshot Handle
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
	// Check if snapshot created is valid
	if (hSnapshot == INVALID_HANDLE_VALUE) return false;

	//Create helper struct that contains all infos of current process
	//while loop thru all running processes
	PROCESSENTRY32 ProcEntry;
	ProcEntry.dwSize = sizeof(PROCESSENTRY32);

	if (Process32First(hSnapshot, &ProcEntry))
	{
		if (!strcmp(ProcEntry.szExeFile, strProcessName.c_str()))
		{
			//oncee here, process has been found
			//firstly close the snapshot handle
			CloseHandle(hSnapshot);
			//Open a handle and set the m_hProcess member using OpenProcess
			m_hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, ProcEntry.th32ProcessID);
			// Store the process id into m_dwProcessId
			m_dwProcessId = ProcEntry.th32ProcessID;
			return true;
		}
	}
	else {
		// If the Process32First call failed, no process running in the first place
		CloseHandle(hSnapshot);
		return false;
	}

	// If here, the Process32First call returned TRUE, but first process wasnt the proc we searching for
	// loop thru the process using Process32Next
	while (Process32Next(hSnapshot, &ProcEntry))
	{
		//Same check as for Process32First
		if (!strcmp(ProcEntry.szExeFile, strProcessName.c_str()))
		{
			//If here process found!
			//Open a handle to it and set m_hProcess member using OpenProcess
			m_hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, ProcEntry.th32ProcessID);
			// Store process id into m_dwProcessId
			m_dwProcessId = ProcEntry.th32ProcessID;
			return true;
		}
	}
	//if here, the process has not been found or! no processes to scan
	// close snapshot handle and return false.
	CloseHandle(hSnapshot);
	return false;
}

// Grabs module and adds it to the m_Modules if found based on strModuleName
bool CMemoryManager::GrabModule(const std::string& strModuleName)
{
	// Create a snapshot handle specific for modules
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, m_dwProcessId);
	// Check if snapshot created is valid
	if (hSnapshot == INVALID_HANDLE_VALUE) return false;

	// Create the helper struct what will contain all infos about current module
	// while looping thru all the loaded modules
	MODULEENTRY32 ModEntry;
	// REMEMBER set the dwSize member of ModEntry to sizeof(MODULEENTRY32)
	ModEntry.dwSize = sizeof(MODULEENTRY32);

	// Call Module32First
	if (Module32First(hSnapshot, &ModEntry))
	{
		if (!strcmp(ModEntry.szModule, strModuleName.c_str()))
		{
			// IF HERE Means module has been found! We can add the module
			// to the vector but first close snapshot handle!
			CloseHandle(hSnapshot);
			// Add ModEntry to the m_modules vector
			m_Modules.push_back(ModEntry); // You can add check here to see if module already there
			return true;
		}
	}
	else {
		// if the process32First call failed, mean that there is no
		// process running in the first place, we can return directly
		CloseHandle(hSnapshot);
		return false;
	}
	while (Module32Next(hSnapshot, &ModEntry))
	{
		// Do same check as Module32First
		if (!strcmp(ModEntry.szModule, strModuleName.c_str()))
		{
			// If here, means the module is found! We can add the module to the vector
			// BUT! First we have to close the Snapshot handle!
			CloseHandle(hSnapshot);
			// Add ModEntry to the m_Modules vector
			m_Modules.push_back(ModEntry);
			return true;
		}
	}
	//Continue loop while Module32Next call returns TRUE, meaning
	// there are still modules to check!

	// If here mean module has not been found or that there are no modules to scan for anymore
	// We can close the snapshot handle and return false!
	CloseHandle(hSnapshot);
	return false;
}





// This constructor will attach to a specific process
CMemoryManager::CMemoryManager(const std::string& strProcessName = "hl2.exe")
{
	// Init Members
	m_hProcess = INVALID_HANDLE_VALUE;
	m_dwProcessId = 0;
	// Safety Precations clear modules vector
	m_Modules.clear();

	// Attach and throw if the function failed so we can manage the error/fail
	if (!Attach(strProcessName)) {
		throw;
	}
}

//Getters
HANDLE CMemoryManager::GetHandle() { return m_hProcess; }
uintptr_t CMemoryManager::GetProcId() { return m_dwProcessId; }
std::vector<MODULEENTRY32> CMemoryManager::GetModules() { return m_Modules; }

