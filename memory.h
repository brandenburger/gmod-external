#include <Windows.h>
#include <iostream>
#include <TlHelp32.h>
#include <vector>

class CMemoryManager;

extern CMemoryManager* MemoryManager;
class CMemoryManager
{

private:
	HANDLE m_hProcess;		//The HANDLE to the process to attach
	uintptr_t m_dwProcessId;	//The Process Id of the process to attach
	std::vector<MODULEENTRY32> m_Modules; // std::vector containing all the modules we grab from the process

public:
	bool Attach(const std::string& strProcessName);

	bool GrabModule(const std::string& strModuleName);

	// This constructor will attach to a specific process

	CMemoryManager() {
		// Init members
		m_hProcess = INVALID_HANDLE_VALUE;
		m_dwProcessId = 0;
		// Safety purposes, Clear out the modules vector
		m_Modules.clear();
	}


	CMemoryManager(const std::string& strProcessName);



	// RPM/WPM wrappers
		// Read a value from memory and put it in Value
		// Returns true on success, False on failure
	template <class T>
	inline bool Read(uintptr_t dwAddress, T& Value)
	{
		return ReadProcessMemory(m_hProcess, reinterpret_cast<LPVOID>(dwAddress), &Value, sizeof(T), NULL) == TRUE;
	}
	// Write a Value in memory
	// Returns true on success, false on failure
	template <class T>
	inline bool Write(uintptr_t dwAddress, const T& Value)
	{
		return WriteProcessMemory(m_hProcess, reinterpret_cast<LPVOID>(dwAddress), &Value, sizeof(T), NULL) == TRUE;
	}

	HANDLE GetHandle();
	uintptr_t GetProcId();
	std::vector<MODULEENTRY32> GetModules();



};

